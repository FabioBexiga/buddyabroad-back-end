const Pool = require('pg').Pool;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const pool = new Pool({
  user: 'me',
  host: 'localhost',
  database: 'buddyabroaddb',
  password: 'password',
  port: 5432,
});
const SECRET_KEY = "secretkey23456";

//Método para obter todas a linguas existentes na base de dados por ordem ascendente
const getLanguages = (request, response) => {
  pool.query('SELECT * FROM "tblLanguages" ORDER BY "languageName" ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

//Método para obter todos os tipos de POI existentes na base de dados por ordem ascendente
const getPOITypes = (request, response) => {
  pool.query('SELECT * FROM "tblPOIType" ORDER BY "poiTypeName" ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

//Método para verificar a existência dum turista com determinado email
const getTouristExistance = (req, res) => {
  const  email  =  req.body.email;
  //console.log(email);
  findTouristByEmail(email, (err, user)=>{
    //console.log(user);
    if (err) return  res.status(500).send('Server error!');
    if (!user.rowCount) return  res.status(200).send({ "user":  false });
    res.status(200).send({ "user":  true });
    });
}

//Método para obtenção de informação do turista (informação pessoal, linguas e interesses)
const getTouristInfo = (request, response) => {
  const email  =  request.body.email;
  pool.query('SELECT * FROM "tblTourist" WHERE "email" = $1',[email], (err, tourist) => {
      //console.log(result.rows);
      if (err) return  res.status(500).send('Server error!');
      pool.query('SELECT "tblTourist_tblLanguages"."languageId" FROM "tblTourist_tblLanguages" WHERE "touristId" = $1',[tourist.rows[0].touristId], (error, touristLanguages) => {
        if (error) {
          throw error
        }
        pool.query('SELECT "tblTouristInterests"."poiTypeId" FROM "tblTouristInterests" WHERE "touristId" = $1',[tourist.rows[0].touristId], (error, touristInterests) => {
          if (error) {
            throw error
          }
          response.status(200).send({ "tourist":  tourist.rows, "languages": touristLanguages.rows, "interests":  touristInterests.rows});
        });
      });
  });
  
}

//Método para verificar o login do turista
const loginTourist = (req, res) => {
	const  email  =  req.body.email;
	const  password  =  req.body.password;
    findTouristByEmail(email, (err, user)=>{
        if (err) return  res.status(500).send('Server error!');
        if (!user.rowCount) return  res.status(404).send('User not found!');
        const result = bcrypt.compareSync(password, user.rows[0].password); //Verificação da password inserida
        if(!result) return  res.status(401).send('Password not valid!');

        const  expiresIn  =  24  *  60  *  60;
        const  accessToken  =  jwt.sign({ id:  user.rows[0].touristId }, SECRET_KEY, { //Criação dum JWT
            expiresIn:  expiresIn
        });
        res.status(200).send({ "user":  user.rows[0], "access_token":  accessToken, "expires_in":  expiresIn});
    });
}

//Método para registo de um turista
const registerTourist = (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const dateOfBirth = req.body.dateOfBirth;
  const mobile = req.body.mobile;
  const email = req.body.email;
  //console.log(req.body);
  const password = bcrypt.hashSync(req.body.password);
  const languages = req.body.languages;
  const interests = req.body.interests;

  
  createTourist([firstName, lastName, dateOfBirth, mobile, email, password], (err, result)=>{
      if(err)
      {
        console.log(err);
        return  res.status(500).send("Server error!");
      } 
      //Caso a inserção do turista foi bem sucedida procede com a inserção dos seus
      //interesses e linguas
      let createdTouristId = result.rows[0].touristId;
      for(let poiTypeId of interests){
        addTouristInterests([createdTouristId, poiTypeId], (err)=>{
          if(err)
            console.log(err);
        });
      }
      for(let languageId of languages){
        addTouristLanguages([createdTouristId, languageId], (err)=>{
          if(err)
            console.log(err);
        });
      }
      //---------------------------------
      findTouristByEmail(email, (err, user)=>{
          if (err) {
            console.log(err);
            return  res.status(500).send('Server error!');  
          }
          const  expiresIn  =  24  *  60  *  60;
          const  accessToken  =  jwt.sign({ id:  user.rows[0].touristId }, SECRET_KEY, {
              expiresIn:  expiresIn
          });
          res.status(200).send({ "user":  user.rows[0], "access_token":  accessToken, "expires_in":  expiresIn          
          });
      });
  });
}

//Método para obter determinado turista através do email
const findTouristByEmail = (email, cb) => {
  return  pool.query('SELECT * FROM "tblTourist" WHERE "email" = $1',[email], (err, result) => {
            cb(err, result)
          });
}

//Método para inserção do registo do turista
const createTourist = (user, cb) => {
  //console.log(user);
  return  pool.query('INSERT INTO "tblTourist" ("firstName", "lastName", "dateOfBirth", mobile, email, password)'+
                  ' VALUES ($1, $2, $3, $4, $5, $6) returning "touristId"',user, (err, row) => {
                        cb(err, row)
          });
}

//Método para inserção dos interesses de determinado turista
const addTouristInterests = ([touristId,poiTypeId], cb) => {
  return pool.query('INSERT INTO "tblTouristInterests" ("touristId", "poiTypeId") VALUES ($1, $2)',[touristId,poiTypeId], (err, row) => {
    cb(err, row)
  });
}

//Método para inserção das linguas de determinado turista
const addTouristLanguages = ([touristId,languageId], cb) => {
  return pool.query('INSERT INTO "tblTourist_tblLanguages" ("touristId", "languageId") VALUES ($1, $2)',[touristId,languageId], (err, row) => {
    cb(err, row)
  });
}

module.exports = {
  getLanguages,
  getPOITypes,
  loginTourist,
  registerTourist,
  getTouristExistance,
  getTouristInfo
}