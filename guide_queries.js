const Pool = require('pg').Pool;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const pool = new Pool({
  user: 'me',
  host: 'localhost',
  database: 'buddyabroaddb',
  password: 'password',
  port: 5432,
});
const SECRET_KEY = "secretkey23456";

//Método para obter todas a linguas existentes na base de dados por ordem ascendente
const getLanguages = (request, response) => {
  pool.query('SELECT * FROM "tblLanguages" ORDER BY "languageName" ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

//Método para obter todos os tipos de POI existentes na base de dados por ordem ascendente
const getPOITypes = (request, response) => {
  pool.query('SELECT * FROM "tblPOIType" ORDER BY "poiTypeName" ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

//Método para verificar a existência dum guia turistico com determinado email
const getGuideExistance = (req, res) => {
  const  email  =  req.body.email;
  //console.log(email);
  findGuideByEmail(email, (err, user)=>{
    //console.log(user);
    if (err) return  res.status(500).send('Server error!');
    if (!user.rowCount) return  res.status(200).send({ "user":  false });
    res.status(200).send({ "user":  true });
    });
}

//Método para obtenção de informação do guia turistico (informação pessoal, linguas e interesses)
const getGuideInfo = (request, response) => {
  const email  =  request.body.email;
  pool.query('SELECT * FROM "tblGuide" WHERE "email" = $1',[email], (err, guide) => {
      //console.log(result.rows);
      if (err) return  res.status(500).send('Server error!');
      pool.query('SELECT "tblGuide_tblLanguages"."languageId" FROM "tblGuide_tblLanguages" WHERE "guideId" = $1',[guide.rows[0].guideId], (error, guideLanguages) => {
        if (error) {
          throw error
        }
        pool.query('SELECT "tblGuideInterests"."poiTypeId" FROM "tblGuideInterests" WHERE "guideId" = $1',[guide.rows[0].guideId], (error, guideInterests) => {
          if (error) {
            throw error
          }
          response.status(200).send({ "guide":  guide.rows, "languages": guideLanguages.rows, "interests":  guideInterests.rows});
        });
      });
  });
  
}

//Método para verificar o login do guia turistico
const loginGuide = (req, res) => {
	const  email  =  req.body.email;
	const  password  =  req.body.password;
    findGuideByEmail(email, (err, user)=>{
        if (err) return  res.status(500).send('Server error!');
        if (!user.rowCount) return  res.status(404).send('User not found!');
        const result = bcrypt.compareSync(password, user.rows[0].password); //Verificação da password inserida
        if(!result) return  res.status(401).send('Password not valid!');

        const  expiresIn  =  24  *  60  *  60;
        const  accessToken  =  jwt.sign({ id:  user.rows[0].touristId }, SECRET_KEY, { //Criação dum JWT
            expiresIn:  expiresIn
        });
        res.status(200).send({ "user":  user.rows[0], "access_token":  accessToken, "expires_in":  expiresIn});
    });
}

//Método para registo de um guia turistico
const registerGuide = (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const dateOfBirth = req.body.dateOfBirth;
  const mobile = req.body.mobile;
  const email = req.body.email;
  //console.log(req.body);
  const password = bcrypt.hashSync(req.body.password);
  const languages = req.body.languages;
  const interests = req.body.interests;

  
  createGuide([firstName, lastName, dateOfBirth, mobile, email, password], (err, result)=>{
      if(err)
      {
        console.log(err);
        return  res.status(500).send("Server error!");
      } 
      //Caso a inserção do guia turistico foi bem sucedida procede com a inserção dos seus
      //interesses e linguas
      let createdGuideId = result.rows[0].guideId;
      for(let poiTypeId of interests){
        addGuideInterests([createdGuideId, poiTypeId], (err)=>{
          if(err)
            console.log(err);
        });
      }
      for(let languageId of languages){
        addGuideLanguages([createdGuideId, languageId], (err)=>{
          if(err)
            console.log(err);
        });
      }
      //---------------------------------
      findGuideByEmail(email, (err, user)=>{
          if (err) {
            console.log(err);
            return  res.status(500).send('Server error!');  
          }
          const  expiresIn  =  24  *  60  *  60;
          const  accessToken  =  jwt.sign({ id:  user.rows[0].guideId }, SECRET_KEY, {
              expiresIn:  expiresIn
          });
          res.status(200).send({ "user":  user.rows[0], "access_token":  accessToken, "expires_in":  expiresIn});
      });
  });
}

//Método para obter determinado guia turistico através do email
const findGuideByEmail = (email, cb) => {
  return  pool.query('SELECT * FROM "tblGuide" WHERE "email" = $1',[email], (err, result) => {
            cb(err, result)
          });
}

//Método para inserção do registo do guia turistico
const createGuide = (user, cb) => {
  //console.log(user);
  return  pool.query('INSERT INTO "tblGuide" ("firstName", "lastName", "dateOfBirth", mobile, email, password)'+
                  ' VALUES ($1, $2, $3, $4, $5, $6) returning "guideId"',user, (err, row) => {
                        cb(err, row)
          });
}

//Método para inserção dos interesses de determinado guia turistico
const addGuideInterests = ([guideId,poiTypeId], cb) => {
  return pool.query('INSERT INTO "tblGuideInterests" ("guideId", "poiTypeId") VALUES ($1, $2)',[guideId,poiTypeId], (err, row) => {
    cb(err, row)
  });
}

//Método para inserção das linguas de determinado guia turistico
const addGuideLanguages = ([guideId,languageId], cb) => {
  return pool.query('INSERT INTO "tblGuide_tblLanguages" ("guideId", "languageId") VALUES ($1, $2)',[guideId,languageId], (err, row) => {
    cb(err, row)
  });
}

module.exports = {
  getLanguages,
  getPOITypes,
  loginGuide,
  registerGuide,
  getGuideExistance,
  getGuideInfo
}