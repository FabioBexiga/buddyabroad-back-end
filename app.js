const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();
const port = 3000;

app.use(router);
router.use(bodyParser.urlencoded({ extended:  false }));
router.use(bodyParser.json());

router.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
});

//Routing para pedidos sobre o turista
const tourist_db = require('./tourist_queries');
router.get('/languages', tourist_db.getLanguages);
router.get('/poitypes', tourist_db.getPOITypes);
router.post('/logintourist', tourist_db.loginTourist);
router.post('/registertourist', tourist_db.registerTourist);
router.post('/touristexist', tourist_db.getTouristExistance);
router.post('/touristinfo', tourist_db.getTouristInfo);

//Routing para pedidos sobre o guia turistico
const guide_db = require('./guide_queries');
router.get('/languages', guide_db.getLanguages);
router.get('/poitypes', guide_db.getPOITypes);
router.post('/loginguide', guide_db.loginGuide);
router.post('/registerguide', guide_db.registerGuide);
router.post('/guideexist', guide_db.getGuideExistance);
router.post('/guideinfo', guide_db.getGuideInfo);

//Inicialização do servidor
app.listen(port, () => {
  console.log(`API Server running on port ${port}.`)
});