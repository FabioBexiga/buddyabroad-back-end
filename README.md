# BuddyAbroad back-end

Protótipo de aplicação em Node.js utilizando a framework Express.js e base de dados PostgreSQL de modo a criar um RESTful API Server

---

## Setup

1. Instalar a versão 10.15.3 LTS do Node.js [Node.js x64](https://nodejs.org/download/release/v10.15.3/node-v10.15.3-x64.msi)
2. Instalar a versão 10 do PostgreSQL [PostgreSQL x64](https://sbp.enterprisedb.com/getfile.jsp?fileid=11693)
3. Abrir linha de comandos / powershell na raiz da pasta e executar os seguintes comandos:
	- "psql -U postgres" e inserir a password (definida aquando da instação do PostgreSQL) para iniciar sessão como super user
	- "CREATE ROLE me WITH LOGIN CREATEDB PASSWORD 'password';" para criar o utilizador a utilizar
		- "\q" para terminar a sessão de super user
	- "psql -d postgres -U me" e inserir a password "password" para iniciar sessão
	- copiar o conteúdo do ficheiro "createdb" e colar na linha de comandos para criação da base de dados e tabelas a utilizar
	- "npm install" para instalação das dependências
	- "node app.js" para executar a aplicação (deverá ser exibida a mensagem 'API Server running on port 3000.')
4. Para terminar a aplicação efectuar ctrl+c
